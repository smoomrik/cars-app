import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import "rxjs/add/operator/map";
import Transmission from "../model/Transmission.model";

@Injectable()
export class TransmissionService {

    constructor(private http: Http) {
    }

    getTransmissions(): Observable<Transmission[]> {
        return this.http.get('http://localhost:8080/api/transmissions')
            .map(response => response.json())
    }
}

