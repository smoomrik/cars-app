/**
 * Created by smoomrik on 03/11/16.
 */
import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";

@Injectable()
export class CarImageService {

    constructor(private http: Http) {}

    getImagesUrlsByStorageUUID(uuid: string): Observable<string[]> {
        let rootPath: string = "http://localhost:8080/api/carImage/" + uuid + "/";
        return this.http.get(rootPath)
            .map(response => this.addRootPathToImageUUID(response, rootPath));
    }

    getImageUrlByAdUUIDandImageUUID(adUUID: string, imageUUID: string): string {
        let rootPath: string = "http://localhost:8080/api/carImage/";
        return rootPath + adUUID + "/" + imageUUID + "/";
    }

    private addRootPathToImageUUID(res: Response, rootPath: string) {
        let uuids = res.json();
        let urls: string[] = [];
        uuids.forEach(uuid => urls.push(rootPath + uuid));
        return urls;

    }

    deleteImage(adUUID: string, imageUUID: string):Observable<Response> {
        return this.http.delete("http://localhost:8080/api/carImage/" + adUUID + "/" + imageUUID);

    }


}