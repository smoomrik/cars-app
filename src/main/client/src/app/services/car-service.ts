import {Injectable, EventEmitter, Inject} from "@angular/core";
import {Http, URLSearchParams, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";
import {IAppConfig, APP_CONFIG} from "../config/app.config";
import Car from "../model/Car.model";

export interface CarSearchParams {
    bodyTypeId: number;
    brandId: string,
    engineMax: number;
    engineMin: number;
    fuelTypeId: number;
    mileageMax: number;
    modelId: string,
    modelYearMax: number;
    modelYearMin: number;
    transmissionTypeId: number;
    wheelDriveId: number;

}

@Injectable()
export class CarService {
    searchEvent: EventEmitter<any> = new EventEmitter();

    constructor(private http: Http, @Inject(APP_CONFIG) private config:IAppConfig) {
    }

    getCars(): Observable<Car[]> {
        return this.http.get(`${this.config.apiEndpoint}/cars`)
            .map(CarService.extractData)
            .catch(CarService.handleError);
    }

    search(params: CarSearchParams): Observable<Car[]> {
        return this.http.get('http://localhost:8080/api/cars', {search: this.encodeParams(params)})
            .map(CarService.extractData).catch(CarService.handleError);
    }

    getCarById(carId: number): Observable<Car> {
        return this.http.get(`http://localhost:8080/api/cars/${carId}`)
            .map(response => response.json());
    }

    saveCar(car: Car):Observable<Response> {
        console.log("Save car:");
        console.log(car);
        return this.http.post(`${this.config.apiEndpoint}/cars/`, car);

    }


    private static extractData(res: Response) {
        let body = res.json();
        return body.content;
    }

    private static handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    private encodeParams(params: any): URLSearchParams {
        return Object.keys(params)
            .filter(key => params[key])
            .reduce((all: URLSearchParams, key: string) => {
                if (!(params[key] === 'null')) {
                    all.append(key, params[key]);
                }
                return all;
            }, new URLSearchParams());
    }
}