import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import "rxjs/add/operator/map";
import Brand from "../model/Brand.model";


@Injectable()
export class BrandService{

    constructor(private http:Http){
    }

    getBrands():Observable<Brand[]>{
        return this.http.get('http://localhost:8080/api/brands')
            .map(response => response.json())
    }
}


