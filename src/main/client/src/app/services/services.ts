import {BrandService} from "./brand-service";
import {CarService} from "./car-service";
import {ModelService} from "./model-service";
import {FuelService} from "./fuel-type-service";
import {BodyService} from "./body-type-service";
import {TransmissionService} from "./transmission-service";
import {WheelDriveService} from "./wheel-drive-service";
import {CarImageService} from "./car-image-service";

export const CARS_APP_SERVICES = [
    BrandService,
    CarService,
    ModelService,
    FuelService,
    BodyService,
    TransmissionService,
    WheelDriveService,
    CarImageService
];