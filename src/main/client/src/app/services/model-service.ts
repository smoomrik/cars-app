import {Observable} from "rxjs";
import {Http} from "@angular/http";
import "rxjs/add/operator/map";
import {Injectable} from "@angular/core";
import CarModel from "../model/CarModel.model";


@Injectable()
export class ModelService {
    constructor(private http: Http) {
    }

    getModelsByBrandId(brandId: number): Observable<CarModel[]> {
        console.log(`getModelsByBrandId(): id  = ${brandId}`);
        return this.http.get(`http://localhost:8080/api/models/${brandId}`)
            .map(response => response.json());
    }
}
