import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import "rxjs/add/operator/map";
import WheelDrive from "../model/WheelDrive.model";

@Injectable()
export class WheelDriveService {

    constructor(private http: Http) {
    }

    getWheelDrives(): Observable<WheelDrive[]> {
        return this.http.get('http://localhost:8080/api/wheels')
            .map(response => response.json())
    }
}
