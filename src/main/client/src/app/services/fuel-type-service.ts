import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import "rxjs/add/operator/map";
import FuelType from "../model/FuelType.model";

@Injectable()
export class FuelService{

    constructor(private http:Http){
    }

    getFuelTypes():Observable<FuelType[]>{
        return this.http.get('http://localhost:8080/api/fuels')
            .map(response => response.json())
    }
}

