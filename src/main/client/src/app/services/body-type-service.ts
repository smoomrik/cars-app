import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import "rxjs/add/operator/map";
import BodyType from "../model/BodyType.model";

@Injectable()
export class BodyService{

    constructor(private http:Http){
    }

    getBodyTypes():Observable<BodyType[]>{
        return this.http.get('http://localhost:8080/api/bodies')
            .map(response => response.json())
    }
}

