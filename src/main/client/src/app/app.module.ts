import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {LocationStrategy, HashLocationStrategy} from "@angular/common";
import {RouterModule} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AppComponent} from "./app.component";
import {NavbarComponent} from "./components/navbar/navbar.component";
import {FooterComponent} from "./components/footer/footer.component";
import {SearchComponent} from "./components/search/search.component";
import CarItemComponent from "./components/car-item/car-item.component";
import {HomeComponent} from "./components/home/home.component";
import {CARS_APP_SERVICES} from "./services/services";
import {CarDetailComponent} from "./components/car-detail/car-detail.component";
import CarouselComponent from "./components/carousel/carousel.component";
import {CarAddComponent} from "./components/car-add/car-add.component";
import {FileUploadModule} from "primeng/components/fileupload/fileupload";
import {InputMaskModule} from "primeng/primeng";
import {SelectButtonModule} from "primeng/components/selectbutton/selectbutton";
import {AppConfig, APP_CONFIG} from "./config/app.config";


@NgModule({
    imports: [
        BrowserModule, HttpModule, ReactiveFormsModule,
        RouterModule.forRoot([
            {path: '', component: HomeComponent},
            {path: 'cars/:carId', component: CarDetailComponent},
            {path: 'add', component: CarAddComponent}
        ]), FileUploadModule, InputMaskModule, SelectButtonModule
    ],
    declarations: [
        AppComponent,
        NavbarComponent,
        FooterComponent,
        SearchComponent,
        CarItemComponent,
        HomeComponent,
        CarDetailComponent,
        CarouselComponent,
        CarAddComponent,
    ],
    providers: [
        {provide: LocationStrategy, useClass: HashLocationStrategy},
        {provide: APP_CONFIG, useValue: AppConfig},
        ...CARS_APP_SERVICES
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
