import {Component, EventEmitter} from "@angular/core";
import {BrandService} from "../../services/brand-service";
import {Observable} from "rxjs";
import {ModelService} from "../../services/model-service";
import {FormGroup, FormBuilder} from "@angular/forms";
import {CarService} from "../../services/car-service";
import {FuelService} from "../../services/fuel-type-service";
import {BodyService} from "../../services/body-type-service";
import {TransmissionService} from "../../services/transmission-service";
import {WheelDriveService} from "../../services/wheel-drive-service";
import Brand from "../../model/Brand.model";
import CarModel from "../../model/CarModel.model";
import FuelType from "../../model/FuelType.model";
import BodyType from "../../model/BodyType.model";
import Transmission from "../../model/Transmission.model";
import WheelDrive from "../../model/WheelDrive.model";


@Component({
    selector: "search-panel",
    templateUrl: "search.component.html",
    styleUrls: ["search.component.css"]
})
export class SearchComponent {

    MODEL_YEAR_MIN: number = 1988;
    MILEAGE_ARRAY: number[] = [
        5000, 10000, 15000, 20000, 30000, 40000, 50000,
        75000, 100000, 150000, 200000, 250000, 300000
    ];


    formModel: FormGroup;
    currentDate: Date = new Date();

    brands: Observable<Brand[]>;
    models: Observable<CarModel[]>;
    fuelTypes: Observable<FuelType[]>;
    bodyTypes: Observable<BodyType[]>;
    transmissions: Observable<Transmission[]>;
    wheelDrives: Observable<WheelDrive[]>;

    brandChangeEvent: EventEmitter<any> = new EventEmitter();

    constructor(private brandService: BrandService,
                private modelService: ModelService,
                private fuelTypeService: FuelService,
                private bodyTypeService: BodyService,
                private transmissionService: TransmissionService,
                private wheelDriveService: WheelDriveService,
                private carService: CarService) {

        const searchForm:any = JSON.parse(localStorage.getItem("searchForm"));
        const fb = new FormBuilder();
        this.formModel = fb.group({
            'brandId': [!searchForm?null:searchForm['brandId']],
            'modelId': [!searchForm?null:searchForm['modelId']],
            'modelYearMin': [!searchForm?this.MODEL_YEAR_MIN:searchForm['modelYearMin']],
            'modelYearMax': [!searchForm?this.currentDate.getFullYear():searchForm['modelYearMax']],
            'mileageMax': [!searchForm?null:searchForm['mileageMax']],
            'engineMin': [!searchForm?null:searchForm['engineMin']],
            'engineMax': [!searchForm?null:searchForm['engineMax']],
            'fuelTypeId': [!searchForm?null:searchForm['fuelTypeId']],
            'bodyTypeId': [!searchForm?null:searchForm['bodyTypeId']],
            'transmissionTypeId': [!searchForm?null:searchForm['transmissionTypeId']],
            'wheelDriveId': [!searchForm?null:searchForm['wheelDriveId']]
        })


        this.brands = brandService.getBrands();

        this.brandChangeEvent
            .subscribe(
                (brandId: number)=>
                    this.models = this.modelService.getModelsByBrandId(brandId),
                console.error.bind(console),
                () => console.log('DONE')
            );
        if (searchForm && searchForm['brandId']&& !(searchForm['brandId'] === 'null')) {
            this.brandChangeEvent.emit(searchForm['brandId']);
        }
        this.bodyTypes = bodyTypeService.getBodyTypes();
        this.fuelTypes = fuelTypeService.getFuelTypes();
        this.transmissions = transmissionService.getTransmissions();
        this.wheelDrives = wheelDriveService.getWheelDrives();
        this.onSearch();

    }

    getArrayOfYears(from: number, to: number): number[] {
        let formModelYearMin: number = this.formModel.controls['modelYearMin'].value;
        let formModelYearMax: number = this.formModel.controls['modelYearMax'].value;
        if (formModelYearMin > formModelYearMax) {
            this.formModel.patchValue({'modelYearMax': formModelYearMin})
        }
        return Array(to - from + 1).fill(0).map((x, i)=>from + i);
    }

    getEnginesCapacity(): number[] {
        return [1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2, 2.2, 2.5, 2.8, 3, 4, 5, 6]
    }

    onBrandChange(brandId) {
        this.formModel.patchValue({'modelId': null});;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        if (!(brandId === 'null' )) {
            this.brandChangeEvent.emit(brandId)
        } else {
            this.formModel.patchValue({'brandId': null})
        }
    }

    onSearch() {
        localStorage.setItem("searchForm", JSON.stringify(this.formModel.value));
        this.carService.searchEvent.emit(this.formModel.value);
    }

    resetForm():void{
        this.formModel.reset();
        this.formModel.patchValue({
            'modelYearMin':this.MODEL_YEAR_MIN,
            'modelYearMax':this.currentDate.getFullYear()
        });;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        localStorage.removeItem("searchForm");
        this.onSearch();
    }
}
