import {Component, Input} from "@angular/core";
import {Observable} from "rxjs";


@Component({
    selector: 'carousel',
    styleUrls: ['carousel.component.css'],
    templateUrl: 'carousel.component.html'
})
export default class CarouselComponent {

    @Input()
    imagesUrls:Observable<string[]>;


}