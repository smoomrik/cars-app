import {Component} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {CarService} from "../../services/car-service";
import {CarImageService} from "../../services/car-image-service";
import {Observable} from "rxjs";
import Car from "../../model/Car.model";


@Component({
    selector: 'car-detail',
    styleUrls: ['car-detail.component.css'],
    templateUrl: 'car-detail.component.html'
})
export class CarDetailComponent {
    car: Car;
    imageStorageUUID: string;
    imagesUrls: Observable<string[]>;

    constructor(private carService: CarService,
                private carImageService: CarImageService,
                router: ActivatedRoute) {
        const carId = parseInt(router.snapshot.params['carId']);

        this.carService.getCarById(carId)
            .subscribe((car:Car) => {
                    this.car = car,
                    this.imageStorageUUID = car.imageStorageUUID;
                    this.imagesUrls = this.carImageService.getImagesUrlsByStorageUUID(this.imageStorageUUID);
                },
                error => console.error(error));
    }

}