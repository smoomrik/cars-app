import {Component, Input} from "@angular/core";
import {DomSanitizer, SafeHtml} from "@angular/platform-browser";
import Car from "../../model/Car.model";

@Component({
    selector:'car-item',
    styleUrls:['car-item.component.css'],
    templateUrl: 'car-item.component.html'
})
export default class CarItemComponent{
    @Input()
    car: Car;

    imgHTML: SafeHtml;

    constructor(private sanitizer:DomSanitizer){
    this.imgHTML = sanitizer.bypassSecurityTrustHtml(`
      <img src="http://placehold.it/220x165">`);
    }

}