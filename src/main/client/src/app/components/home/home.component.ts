import {Observable} from "rxjs";
import {CarService} from "../../services/car-service";
import {Component} from "@angular/core";
import Car from "../../model/Car.model";


@Component({
    selector: 'cars-home-page',
    template: `
    <div class="row">
        <div class="col-md-2">
            <search-panel></search-panel>
        </div>
        <div class="col-md-10">
          <div class="container-car-item">
                <div *ngFor="let car of cars | async" >
                    <car-item [car]="car" ></car-item>
                </div>
             </div>
        </div>
    </div>
    `,
    styleUrls: ['home.component.css']
})
export class HomeComponent {
    cars: Observable<Car[]>;

    constructor(private carService: CarService) {
        this.cars = this.carService.getCars();
        this.carService.searchEvent.subscribe(
            params=>this.cars = this.carService.search(params),
            console.error.bind(console),
            ()=>console.log('DONE')
        );

    }

}