import {Component, EventEmitter, ViewEncapsulation, OnInit} from "@angular/core";
import UUID from "../../utils/UUID";
import {BrandService} from "../../services/brand-service";
import {Observable} from "rxjs";
import {ModelService} from "../../services/model-service";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {BodyService} from "../../services/body-type-service";
import {FuelService} from "../../services/fuel-type-service";
import {SelectItem} from "primeng/components/common/api";
import {TransmissionService} from "../../services/transmission-service";
import {WheelDriveService} from "../../services/wheel-drive-service";
import {CarImageService} from "../../services/car-image-service";
import {createOneImageValidator, createModelYearValidator} from "../../validators/car.validators";
import {Response} from "@angular/http";
import {CarService} from "../../services/car-service";
import Brand from "../../model/Brand.model";
import CarModel from "../../model/CarModel.model";
import BodyType from "../../model/BodyType.model";
import FuelType from "../../model/FuelType.model";
import Transmission from "../../model/Transmission.model";
import WheelDrive from "../../model/WheelDrive.model";

@Component({
    selector: 'car-add',
    styleUrls: ['car-add.component.css'],
    templateUrl: 'car-add.component.html',
    encapsulation: ViewEncapsulation.None
})

export class CarAddComponent implements OnInit {

    adUUID: string = '';
    imagesUUIDS: string[];
    brands: Observable<Brand[]>;
    brandChangeEvent: EventEmitter<any> = new EventEmitter();
    models: Observable<CarModel[]>;
    formModel: FormGroup;
    bodyTypes: Observable<BodyType[]>;
    fuelTypes: Observable<FuelType[]>;
    transmissionsTypes: SelectItem[] = [];
    wheelDriveTypes: SelectItem[] = [];
    formErrors = {
        'brand': '',
        'model': '',
        'body': '',
        'fuel': '',
        'engineCapacity': '',
        'mileage': '',
        'transmission': '',
        'wheelDrive': '',
        'price': '',
        'modelYear': '',
        'imageStorageUUID': '',

    };
    validationMessages = {
        'brand': {
            'required': 'Выберите марку.'
        },
        'model': {
            'required': 'Выберите модель'
        },
        'body': {
            'required': 'Выберите тип кузова.'
        },
        'fuel': {
            'required': 'Выберите тип топлива.'
        },
        'engineCapacity': {
            'required': 'Введите обьем двигателя в кубических сантиметрах от 100 до 9999.'
        },
        'mileage': {
            'required': 'Введите пробег.'
        },
        'transmission': {
            'required': 'Выберите тип трансмиссии.'
        },
        'wheelDrive': {
            'required': 'Выберите тип привода.'
        },
        'price': {
            'required': 'Введите стоимость.'
        },
        'modelYear': {
            'RangeError': 'Год выпуска может быть от 1980 до ' + new Date().getUTCFullYear() + '.',
            'required': 'Введите год выпуска.',
            'minlength': 'Введите год в формате "2016".',
            'maxlength': 'Введите год в формате "2016".'
        },
        'imageStorageUUID': {
            'required': 'Загрузите как минимум одну фотографию.',
            'upload': 'Ошибка загрузки. '
        }
    };

    constructor(private brandService: BrandService,
                private modelService: ModelService,
                private bodyTypeService: BodyService,
                private fuelTypeService: FuelService,
                private transmissionService: TransmissionService,
                private wheelDriveService: WheelDriveService,
                private carImageService: CarImageService,
                private carService: CarService,
                private fb: FormBuilder) {
    }

    ngOnInit(): void {
        this.adUUID = UUID.generateUUID();
        this.imagesUUIDS = [];


        this.brands = this.brandService.getBrands();
        this.brandChangeEvent
            .subscribe(
                (brandId: number) =>
                    this.models = this.modelService.getModelsByBrandId(brandId),
                console.error.bind(console)
            );
        this.bodyTypes = this.bodyTypeService.getBodyTypes();
        this.fuelTypes = this.fuelTypeService.getFuelTypes();
        this.transmissionService.getTransmissions().subscribe(
            (transmissions: Transmission[]) => {
                transmissions.forEach(
                    t => this.transmissionsTypes.push({label: t.name, value: t.id})
                )
            },
            console.error.bind(console));

        this.wheelDriveService.getWheelDrives().subscribe(
            (wheelDrives: WheelDrive[]) => {
                wheelDrives.forEach(
                    w => this.wheelDriveTypes.push({label: w.name, value: w.id})
                )
            },
            console.error.bind(console));


        this.buildForm();
    }

    private buildForm() {
        this.formModel = this.fb.group({

            'brand': this.fb.group({
                'id': [null, [Validators.required,]]
            }),
            'model': this.fb.group({
                'id': [null, [Validators.required]],
            }),
            'modelYear': ['', [Validators.required, createModelYearValidator(), Validators.minLength(4), Validators.maxLength(4)]],
            'body': this.fb.group({
                'id': [null, [Validators.required]],
            }),
            'fuel': this.fb.group({
                'id' : [null, [Validators.required]]
            }),
            'engineCapacity': ['', [Validators.required]],
            'mileage': ['', [Validators.required]],
            'transmission': this.fb.group({
               'id' :  ['', [Validators.required]]
            }),
            'wheelDrive': this.fb.group({
                'id' : ['', [Validators.required]]
            }),
            'price': ['', [Validators.required]],
            'imageStorageUUID': [this.adUUID, null, createOneImageValidator(this.carImageService, this.adUUID)]
        });
        this.formModel.valueChanges
            .subscribe(data => this.onValueChanged());
        this.onValueChanged();
    }

    onValueChanged() {

        if (!this.formModel) {
            return;
        }
        const form = this.formModel;

        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);
            if (control && !control.valid && (control.dirty || control.touched )) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    onSubmitMouseEnter() {
        //final check validity
        if (!this.formModel) {
            return;
        }
        const form = this.formModel;

        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    onSubmit(): void {
        console.log(this.formModel.value);
        this.carService.saveCar(this.formModel.value).subscribe(
            data => {
                console.log(data);
            },
            error => {
                console.error.bind(console);
            }
        )

    }

    onSelectBlur(formControlName: string) {
        this.formModel.controls[formControlName].markAsDirty(true);
        this.formModel.controls[formControlName].updateValueAndValidity();
    }

    onBeforeUpload(event) {
        event.xhr.setRequestHeader("UUID", this.adUUID);
    }

    onUpload(event) {
        JSON.parse(event.xhr.response).forEach(uuid => this.imagesUUIDS.push(uuid));
        this.formModel.controls['imageStorageUUID'].markAsDirty(true);
        this.formModel.controls['imageStorageUUID'].updateValueAndValidity();
    }

    onUploadError(event) {
        //todo: separate upload errors to files
        console.log(event);
    }

    onBrandChange(brandId) {
        console.log("brandChange: ", brandId);
        this.brandChangeEvent.emit(brandId);
    }

    getImageUrl(imageUUID: string): string {
        return this.carImageService.getImageUrlByAdUUIDandImageUUID(this.adUUID, imageUUID);
    }

    deleteImage(imageUUID: string) {
        this.carImageService.deleteImage(this.adUUID, imageUUID).subscribe(
            (res: Response) => {
                if (res && res.ok) {
                    this.formModel.controls['imageStorageUUID'].updateValueAndValidity();
                }
            },
            error => {
                console.error.bind(console);
                this.formModel.controls['imageStorageUUID'].setErrors({
                    'upload': true
                });
            },
            () => {
                console.log('Deleted');
            }
        );
        let index = imageUUID.indexOf(imageUUID, 0);
        if (index > -1) {
            this.imagesUUIDS.splice(index, 1);
        }
    }

}