import {CarImageService} from "../services/car-image-service";

export function createOneImageValidator(carImageService: CarImageService, adUUID: string) {
    return function (control) {
        return new Promise(resolve =>
            carImageService.getImagesUrlsByStorageUUID(adUUID).subscribe(
                data => {
                    if (data.length > 0) {
                        resolve(null);
                    } else {
                        resolve({'required': true});
                    }
                },
                err => {
                    resolve({'required': true});
                }
            )
        );
    }
}

export function createModelYearValidator() {
    return function (control): {[key: string]: any} {
        const value: number = Number.parseInt(control.value);
        const year: number = new Date().getUTCFullYear();
        return (value > year || value < 1980) ? {'RangeError': true} : null;
    }
}
