export default class FuelType{
    constructor(
        public id:number,
        public name:string
    ){}
}