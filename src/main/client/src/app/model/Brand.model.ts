export default class Brand{
    constructor(
        public id: number,
        public name:string
    ){}
}