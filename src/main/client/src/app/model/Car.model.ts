import Brand from "./Brand.model";
import CarModel from "./CarModel.model";
import BodyType from "./BodyType.model";
import FuelType from "./FuelType.model";
import Transmission from "./Transmission.model";
import WheelDrive from "./WheelDrive.model";
export default class Car {
    constructor(public id: number,
                public brand: Brand,
                public model: CarModel,
                public body: BodyType,
                public modelYear: Date,
                public mileage: number,
                public fuel: FuelType,
                public engineCapacity: number,
                public transmission: Transmission,
                public wheelDrive: WheelDrive,
                public description: string,
                public publishDate: Date,
                public imageStorageUUID: string,
                public price:number,

    ){}

}