var webpackMerge = require('webpack-merge');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');

module.exports = webpackMerge(commonConfig, {
    devtool: 'cheap-module-eval-source-map',
    module: {
        loaders: [
            {
                test: /\.(png|jpe?g|gif|ico)$/,
                loader: 'file?name=resources/images/[name].[ext]'
            },
            {
                test: /\.(svg|woff|woff2|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file?name=resources/fonts/[name].[ext]'
            }
        ]
    },
    output: {
        path: helpers.root('/'),
        publicPath: 'http://localhost:8000/',
        filename: 'resources/js/[name].js',
        chunkFilename: 'resources/js/[id].chunk.js'
    },

    plugins: [
        new ExtractTextPlugin('resources/css/[name].css'),
        new HtmlWebpackPlugin({
            filename: "./index.html",
            template: 'src/index.html'
        })
    ],

    devServer: {
        historyApiFallback: true,
        stats: 'minimal'
    }
});
