var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');

const ENV = process.env.NODE_ENV = process.env.ENV = 'production';

module.exports = webpackMerge(commonConfig, {
    devtool: 'source-map',

    module: {
        loaders: [
            {
                test: /\.(png|jpe?g|gif|ico)$/,
                loader: 'file?name=resources/images/[name].[ext]'
            },
            {
                test: /\.(svg|woff|woff2|ttf|eot|otf)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file?name=resources/fonts/[name].[ext]'
            }
        ]
    },


    output: {
        path: helpers.root('../webapp/'),
        publicPath: '/',
        filename: 'resources/js/[name].js',
        chunkFilename: 'resources/js/[id].chunk.js'
    },

    htmlLoader: {
        minimize: false // workaround for ng2
    },

    plugins: [
        new webpack.NoErrorsPlugin(),
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin({ // https://github.com/angular/angular/issues/10618
            mangle: {
                keep_fnames: true
            }
        }),
        new HtmlWebpackPlugin({
            filename: "static/index.html",
            template: 'src/index.html'
        }),
        new ExtractTextPlugin('resources/css/[name].css'),
        new webpack.DefinePlugin({
            'process.env': {
                'ENV': JSON.stringify(ENV)
            }
        })
    ]
});
