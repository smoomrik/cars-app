package ru.smoomrik.carsapp.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.smoomrik.carsapp.service.CarImageService;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by smoomrik on 28/10/16.
 */
@RestController
class CarImageController {
    private final static Logger log = LoggerFactory.getLogger(CarImageController.class);

    @Autowired
    private CarImageService carImageService;



    @RequestMapping(value = "/api/CarImageUpload", method = RequestMethod.POST)
    public List<String> uploadImage(@RequestParam(name = "file[]") MultipartFile[] files , @RequestHeader("UUID") String uuid){
        ArrayList<String> uuids = new ArrayList<>();
        for (MultipartFile file : files) {
            log.debug("uploadImage():\n\tadUUID: {}\n\tname: {}, content-type: {}, length: {}", uuid, file.getOriginalFilename(),file.getContentType(), file.getSize());
            try(InputStream is = file.getInputStream()){
                String imageUUID = processFile(uuid, FileCopyUtils.copyToByteArray(is));
                if (imageUUID !=null){
                    uuids.add(imageUUID);
                }
            }catch (IOException e){
                log.error(e.getMessage(), e);
                throw new IllegalStateException("Error while uploading file " + file.getOriginalFilename());
            }
        }
        return uuids;
    }

    @RequestMapping(value = "/api/carImage/{adUUID}/{imageUUID}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getImage(@PathVariable(value = "adUUID") String adUUID, @PathVariable(value = "imageUUID") String imageUUID){
        log.debug("getImage: adUUID: {}, imageUUID: {}", adUUID, imageUUID);
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());
        try {
            byte[] image = carImageService.getImageByAdUUIDAndImageUUID(adUUID, imageUUID);
            log.debug("Image retrieved. image length {}",image.length);
            return new ResponseEntity<>(
                    image,
                    headers,
                    HttpStatus.OK);
        } catch (FileNotFoundException e) {
            return new ResponseEntity<>(null,headers,HttpStatus.NOT_FOUND);
        }
    }
    @RequestMapping(value = "/api/carImage/{adUUID}/{imageUUID}", method = RequestMethod.DELETE)
    public ResponseEntity deleteImage(@PathVariable(value = "adUUID") String adUUID, @PathVariable(value = "imageUUID") String imageUUID){
        log.debug("deleteImage: adUUID: {}, imageUUID: {}", adUUID, imageUUID);
        try {
            carImageService.deleteImage(adUUID, imageUUID);
            log.debug("Image deleted: imageUUID = {}",imageUUID);
            return new ResponseEntity<>(imageUUID,
                    HttpStatus.OK);
        } catch (FileNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (IllegalStateException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/api/carImage/{adUUID}", method = RequestMethod.GET)
    public ResponseEntity<List<String>> getImagesUUIDSbyAdUUID(@PathVariable(value = "adUUID") String adUUID){
        log.debug("getImagesUUIDSbyAdUUID(): adUUID = {}", adUUID);
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());
        try{
            List<String> uuids = carImageService.getImagesUUIDByAdUUID(adUUID);
            return new ResponseEntity<>(
                    uuids,
                    headers,
                    HttpStatus.OK);
        } catch (IllegalArgumentException e){
            log.error("Error: ", e);
            return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
        }
    }

    @Async
    private String processFile(String uuid, byte[] bytes)
    {
        log.debug("processFile()");
        return carImageService.addImage(uuid,bytes);
    }

}
