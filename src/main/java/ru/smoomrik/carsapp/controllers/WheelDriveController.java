package ru.smoomrik.carsapp.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.smoomrik.carsapp.model.entities.WheelDrive;
import ru.smoomrik.carsapp.service.WheelDriveService;

import java.util.List;

/**
 * Created by smoomrik on 16/09/16.
 */
@RestController
public class WheelDriveController {
    private static final Logger log = LoggerFactory.getLogger(WheelDriveController.class);

    @Autowired
    private WheelDriveService wheelDriveService;

    @RequestMapping(value = "/api/wheels", method = RequestMethod.GET)
    public List<WheelDrive> getAllWheelDriveTypes(){
        log.debug("getAllWheelDriveTypes()");
        return wheelDriveService.findAllWheelDrive();
    }


}
