package ru.smoomrik.carsapp.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.smoomrik.carsapp.model.entities.Car;
import ru.smoomrik.carsapp.model.repositories.CarRepository;
import ru.smoomrik.carsapp.model.repositories.specification.CarSpecificationBuilder;
import ru.smoomrik.carsapp.service.CarService;

import java.util.Map;

/**
 * Created by smoomrik on 09/09/16.
 */
@RestController
public class CarController {
    private static final Logger log = LoggerFactory.getLogger(CarController.class);

    @Autowired
    private CarService carService;
    @Autowired
    private CarRepository carRepository;


    @RequestMapping(value = "/api/cars", method = RequestMethod.GET)
    public Page<Car> getCars(@RequestParam(required = false) Map<String, String> params) {
        log.debug("getCars():\n\t{}", params);

        Integer page = params.get("page") == null ? null : Integer.valueOf(params.get("page"));
        Integer size = params.get("size") == null ? null : Integer.valueOf(params.get("size"));
        PageRequest pageRequest = new PageRequest(page == null ? 0 : page, size == null ? 20 : size);

        return carRepository.findAll(new CarSpecificationBuilder().buildWith(params), pageRequest);

    }

    @RequestMapping("/api/cars/{id}")
    public Car getCarById(@PathVariable("id") Long id) {
        log.debug("getCarById(): id = {}", id);
        return carService.getCarById(id).orElseThrow(() -> new CarNotFoundException(id));
    }

    @RequestMapping(value = "/api/cars/", method = RequestMethod.POST)
    public ResponseEntity<Car> saveCar(@RequestBody Car car) {
        log.debug("saveCar():");
        log.debug(car.toString());
        Car stored = carService.saveCar(car);
        return new ResponseEntity<Car>(stored, HttpStatus.CREATED);
    }


    @ResponseStatus(HttpStatus.NOT_FOUND)
    private static class CarNotFoundException extends RuntimeException {
        CarNotFoundException(Long id) {
            super("Could not find car with id = " + id);
        }
    }
}
