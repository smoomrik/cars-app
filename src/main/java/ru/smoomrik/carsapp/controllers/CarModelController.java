package ru.smoomrik.carsapp.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.smoomrik.carsapp.model.entities.CarModel;
import ru.smoomrik.carsapp.service.CarModelService;

import java.util.List;

/**
 * Created by smoomrik on 16/09/16.
 */
@RestController
public class CarModelController {
    private static final Logger log = LoggerFactory.getLogger(CarModelController.class);


    @Autowired
    private CarModelService carModelService;

    @RequestMapping(value = "/api/models", method = RequestMethod.GET)
    public List<CarModel> getAllModels() {
        log.debug("getAllModels()");
        return carModelService.findAllModels();

    }

    @RequestMapping(value = "/api/models/{id}")
    public List<CarModel> getModelsByBrandId(@PathVariable("id") Long brandId) {
        log.debug("getModelsByBrandId(): id = {}", brandId);
        if (brandId == -1) {
            return carModelService.findAllModels();
        } else {
            return carModelService.findAllModelsByBrandId(brandId);
        }
    }
}
