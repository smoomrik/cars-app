package ru.smoomrik.carsapp.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.smoomrik.carsapp.model.entities.Fuel;
import ru.smoomrik.carsapp.service.FuelService;

import java.util.List;

/**
 * Created by smoomrik on 16/09/16.
 */
@RestController
public class FuelController {
    private static final Logger log = LoggerFactory.getLogger(FuelController.class);

    @Autowired
    private FuelService fuelService;

    @RequestMapping(value = "/api/fuels", method = RequestMethod.GET)
    public List<Fuel> getAllFuelType(){
        log.debug("getAllFuelType()");
        return fuelService.findAllFuel();
    }

}
