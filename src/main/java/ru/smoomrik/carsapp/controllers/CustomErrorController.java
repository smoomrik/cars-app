package ru.smoomrik.carsapp.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by smoomrik on 09/09/16.
 */
@ControllerAdvice
public class CustomErrorController {
    private Logger log = LoggerFactory.getLogger(CustomErrorController.class);


    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoHandlerFoundException.class)
    public String handle(Exception ex) {
        log.debug("Exception message: " + ex.getLocalizedMessage());
        return "/static/errors/404.html";
    }
}
