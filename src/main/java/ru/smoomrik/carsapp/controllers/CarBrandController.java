package ru.smoomrik.carsapp.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.smoomrik.carsapp.model.entities.CarBrand;
import ru.smoomrik.carsapp.service.CarBrandService;

import java.util.List;

/**
 * Created by smoomrik on 16/09/16.
 */
@RestController
public class CarBrandController {
    private static final Logger log = LoggerFactory.getLogger(CarBrandController.class);

    @Autowired
    private CarBrandService carBrandService;

    @RequestMapping(value = "/api/brands", method = RequestMethod.GET)
    public List<CarBrand> getAllBrands(){
        log.debug("getAllBrands()");
        return carBrandService.findAllBrands();
    }

}
