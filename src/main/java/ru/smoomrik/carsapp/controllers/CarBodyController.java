package ru.smoomrik.carsapp.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.smoomrik.carsapp.model.entities.CarBody;
import ru.smoomrik.carsapp.service.CarBodyService;

import java.util.List;

/**
 * Created by smoomrik on 16/09/16.
 */
@RestController
public class CarBodyController {
    private static final Logger log = LoggerFactory.getLogger(CarBodyController.class);

    @Autowired
    private CarBodyService carBodyService;

    @RequestMapping(value = "/api/bodies", method = RequestMethod.GET)
    public List<CarBody> getAllCarBody(){
        log.debug("getAllCarBody()");
        return carBodyService.findAllCarBody();
    }


}
