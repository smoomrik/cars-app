package ru.smoomrik.carsapp.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.smoomrik.carsapp.model.entities.Transmission;
import ru.smoomrik.carsapp.service.TransmissionService;

import java.util.List;

/**
 * Created by smoomrik on 16/09/16.
 */
@RestController
public class TransmissionController {
    private static final Logger log = LoggerFactory.getLogger(TransmissionController.class);

    @Autowired
    private TransmissionService transmissionService;

    @RequestMapping(value = "/api/transmissions", method = RequestMethod.GET)
    public List<Transmission> getAllTransmission(){
        log.debug("getAllTransmission()");
        return transmissionService.findAllTransmission();
    }


}
