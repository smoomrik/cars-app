package ru.smoomrik.carsapp.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by smoomrik on 16/09/16.
 */
@Entity
@Table(name = "brand", schema = "public", catalog = "carsapp")
public class CarBrand {
    private long id;
    private String name;
    @JsonIgnore
    private Collection<CarModel> models;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 20)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CarBrand carBrand = (CarBrand) o;

        if (id != carBrand.id) return false;
        if (name != null ? !name.equals(carBrand.name) : carBrand.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CarBrand{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @OneToMany(mappedBy = "brand")
    public Collection<CarModel> getModels() {
        return models;
    }

    public void setModels(Collection<CarModel> models) {
        this.models = models;
    }
}
