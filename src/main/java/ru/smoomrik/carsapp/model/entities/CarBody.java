package ru.smoomrik.carsapp.model.entities;

import javax.persistence.*;

/**
 * Created by smoomrik on 16/09/16.
 */
@Entity
@Table(name = "body_type", schema = "public", catalog = "carsapp")
public class CarBody {
    private long id;
    private String name;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 20)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CarBody carBody = (CarBody) o;

        return name != null ? name.equals(carBody.name) : carBody.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "CarBody{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
