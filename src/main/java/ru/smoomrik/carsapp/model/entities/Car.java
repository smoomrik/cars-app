package ru.smoomrik.carsapp.model.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * Created by smoomrik on 16/09/16.
 */
@Entity
public class Car {
    private Integer modelYear;
    private int mileage;
    private Integer engineCapacity;
    private Long id;
    private Date publishDate;
    private String description;
    private CarBrand brand;
    private CarModel model;
    private Fuel fuel;
    private CarBody body;
    private Transmission transmission;
    private WheelDrive wheelDrive;
    private String imageStorageUUID;
    private BigDecimal price;


    @Basic
    @Column(name = "price", nullable = false, precision = 19, scale = 2)
    public BigDecimal getPrice() {return price;}
    public void setPrice(BigDecimal price) {this.price = price;}

    @Basic
    @Column(name= "image_storage_uuid", nullable = false)
    public String getImageStorageUUID() {return imageStorageUUID;}
    public void setImageStorageUUID(String imageStorageUUID) {
        this.imageStorageUUID = imageStorageUUID;
    }

    @Basic
    @Column(name = "model_year", nullable = false)
    public Integer getModelYear() {
        return modelYear;
    }
    public void setModelYear(Integer modelYear) {
        this.modelYear = modelYear;
    }

    @Basic
    @Column(name = "mileage", nullable = false)
    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    @Basic
    @Column(name = "engine_capacity", nullable = false)
    public Integer getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(Integer engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "publish_date", nullable = false)
    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    @Basic
    @Column(name = "description", length = 1500)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (mileage != car.mileage) return false;
        if (modelYear != null ? !modelYear.equals(car.modelYear) : car.modelYear != null) return false;
        if (engineCapacity != null ? !engineCapacity.equals(car.engineCapacity) : car.engineCapacity != null)
            return false;
        if (publishDate != null ? !publishDate.equals(car.publishDate) : car.publishDate != null) return false;
        if (description != null ? !description.equals(car.description) : car.description != null) return false;
        if (brand != null ? !brand.equals(car.brand) : car.brand != null) return false;
        if (model != null ? !model.equals(car.model) : car.model != null) return false;
        if (fuel != null ? !fuel.equals(car.fuel) : car.fuel != null) return false;
        if (body != null ? !body.equals(car.body) : car.body != null) return false;
        if (transmission != null ? !transmission.equals(car.transmission) : car.transmission != null) return false;
        if (wheelDrive != null ? !wheelDrive.equals(car.wheelDrive) : car.wheelDrive != null) return false;
        if (imageStorageUUID != null ? !imageStorageUUID.equals(car.imageStorageUUID) : car.imageStorageUUID != null)
            return false;
        return price != null ? price.equals(car.price) : car.price == null;
    }

    @Override
    public int hashCode() {
        int result = modelYear != null ? modelYear.hashCode() : 0;
        result = 31 * result + mileage;
        result = 31 * result + (engineCapacity != null ? engineCapacity.hashCode() : 0);
        result = 31 * result + (publishDate != null ? publishDate.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (brand != null ? brand.hashCode() : 0);
        result = 31 * result + (model != null ? model.hashCode() : 0);
        result = 31 * result + (fuel != null ? fuel.hashCode() : 0);
        result = 31 * result + (body != null ? body.hashCode() : 0);
        result = 31 * result + (transmission != null ? transmission.hashCode() : 0);
        result = 31 * result + (wheelDrive != null ? wheelDrive.hashCode() : 0);
        result = 31 * result + (imageStorageUUID != null ? imageStorageUUID.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Car{" +
                "modelYear=" + modelYear +
                ", mileage=" + mileage +
                ", engineCapacity=" + engineCapacity +
                ", id=" + id +
                ", publishDate=" + publishDate +
                ", description='" + description + '\'' +
                ", brand=" + brand +
                ", model=" + model +
                ", fuel=" + fuel +
                ", body=" + body +
                ", transmission=" + transmission +
                ", wheelDrive=" + wheelDrive +
                ", imageStorageUUID='" + imageStorageUUID + '\'' +
                ", price=" + price +
                '}';
    }

    @ManyToOne
    @JoinColumn(name = "brand_id", referencedColumnName = "id", nullable = false)
    public CarBrand getBrand() {
        return brand;
    }

    public void setBrand(CarBrand brand) {
        this.brand = brand;
    }

    @ManyToOne
    @JoinColumn(name = "model_id", referencedColumnName = "id", nullable = false)
    public CarModel getModel() {
        return model;
    }

    public void setModel(CarModel model) {
        this.model = model;
    }

    @ManyToOne
    @JoinColumn(name = "fuel_type_id", referencedColumnName = "id", nullable = false)
    public Fuel getFuel() {
        return fuel;
    }

    public void setFuel(Fuel fuel) {
        this.fuel = fuel;
    }

    @ManyToOne
    @JoinColumn(name = "body_type_id", referencedColumnName = "id", nullable = false)
    public CarBody getBody() {
        return body;
    }

    public void setBody(CarBody body) {
        this.body = body;
    }

    @ManyToOne
    @JoinColumn(name = "transmission_type_id", referencedColumnName = "id", nullable = false)
    public Transmission getTransmission() {
        return transmission;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    @ManyToOne
    @JoinColumn(name = "wheel_drive_id", referencedColumnName = "id", nullable = false)
    public WheelDrive getWheelDrive() {
        return wheelDrive;
    }

    public void setWheelDrive(WheelDrive wheelDrive) {
        this.wheelDrive = wheelDrive;
    }



}
