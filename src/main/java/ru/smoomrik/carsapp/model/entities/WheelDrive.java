package ru.smoomrik.carsapp.model.entities;

import javax.persistence.*;

/**
 * Created by smoomrik on 16/09/16.
 */
@Entity
@Table(name = "wheel_drive_type", schema = "public", catalog = "carsapp")
public class WheelDrive {
    private long id;
    private String name;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 15)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WheelDrive that = (WheelDrive) o;

        if (id != that.id) return false;
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "WheelDrive{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
