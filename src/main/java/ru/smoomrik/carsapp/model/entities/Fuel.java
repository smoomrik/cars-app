package ru.smoomrik.carsapp.model.entities;

import javax.persistence.*;

/**
 * Created by smoomrik on 16/09/16.
 */
@Entity
@Table(name = "fuel_type", schema = "public", catalog = "carsapp")
public class Fuel {
    private long id;
    private String name;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 15)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Fuel fuel = (Fuel) o;

        if (id != fuel.id) return false;
        if (name != null ? !name.equals(fuel.name) : fuel.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Fuel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
