package ru.smoomrik.carsapp.model.repositories;

import org.springframework.data.repository.Repository;
import ru.smoomrik.carsapp.model.entities.CarModel;

import java.util.List;
import java.util.Optional;

/**
 * Created by smoomrik on 16/09/16.
 */
public interface CarModelRepository extends Repository<CarModel, Long> {
    List<CarModel> findAll();
    List<CarModel> findAllCarModelByBrandId(Long id);
    Optional<CarModel> findById(Long id);
}
