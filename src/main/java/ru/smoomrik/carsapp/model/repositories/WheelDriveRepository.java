package ru.smoomrik.carsapp.model.repositories;

import org.springframework.data.repository.Repository;
import ru.smoomrik.carsapp.model.entities.WheelDrive;

import java.util.List;
import java.util.Optional;

/**
 * Created by smoomrik on 16/09/16.
 */
public interface WheelDriveRepository extends Repository<WheelDrive, Long>{

    List<WheelDrive> findAll();
    Optional<WheelDrive> findById(Long id);
}
