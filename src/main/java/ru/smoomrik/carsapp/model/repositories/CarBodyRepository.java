package ru.smoomrik.carsapp.model.repositories;

import org.springframework.data.repository.Repository;
import ru.smoomrik.carsapp.model.entities.CarBody;

import java.util.List;
import java.util.Optional;

/**
 * Created by smoomrik on 16/09/16.
 */
public interface CarBodyRepository extends Repository<CarBody, Long>{
    List<CarBody> findAll();
    Optional<CarBody> findById(long id);
}
