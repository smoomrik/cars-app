package ru.smoomrik.carsapp.model.repositories;

import org.springframework.data.repository.Repository;
import ru.smoomrik.carsapp.model.entities.Fuel;

import java.util.List;
import java.util.Optional;

/**
 * Created by smoomrik on 16/09/16.
 */
public interface FuelRepository extends Repository<Fuel, Long>{
    List<Fuel> findAll();
    Optional<Fuel> findById(Long id);
}
