package ru.smoomrik.carsapp.model.repositories;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.smoomrik.carsapp.model.entities.Car;

import java.util.Optional;

/**
 * Created by smoomrik on 16/09/16.
 */

public interface CarRepository extends PagingAndSortingRepository<Car, Long>, JpaSpecificationExecutor<Car> {
    Page<Car> findAll(Specification<Car> specification, Pageable pageRequest);
    Optional<Car> findCarById(Long id);
}
