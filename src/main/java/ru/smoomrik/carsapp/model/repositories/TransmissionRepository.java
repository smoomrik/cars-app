package ru.smoomrik.carsapp.model.repositories;

import org.springframework.data.repository.Repository;
import ru.smoomrik.carsapp.model.entities.Transmission;

import java.util.List;
import java.util.Optional;

/**
 * Created by smoomrik on 16/09/16.
 */
public interface TransmissionRepository extends Repository<Transmission, Long> {
    List<Transmission> findAll();

    Optional<Transmission> findById(Long id);
}
