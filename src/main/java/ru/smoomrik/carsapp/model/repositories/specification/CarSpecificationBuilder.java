package ru.smoomrik.carsapp.model.repositories.specification;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import ru.smoomrik.carsapp.model.entities.Car;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.smoomrik.carsapp.model.repositories.specification.CarSpecifications.*;

/**
 * Created by smoomrik on 07/10/16.
 */
public class CarSpecificationBuilder {
    private final List<Specification<Car>> specifications;
    private Map<String, String> params;

    public CarSpecificationBuilder() {
        specifications = new ArrayList<>();
        params = new HashMap<>();
    }

    public CarSpecificationBuilder add(Specification<Car> specification) {
        specifications.add(specification);
        return this;
    }

    public Specification<Car> build() {
        if (specifications.size() == 0) {
            return null;
        }
        Specification<Car> result = specifications.get(0);
        for (int i = 1; i < specifications.size(); i++) {
            result = Specifications.where(result).and(specifications.get(i));
        }
        return result;
    }

    public Specification<Car> buildWith(Map<String, String> par){
        if (par == null) throw new IllegalArgumentException("map of params can't be null");
        params = par;
        CarSpecificationBuilder specBuilder = new CarSpecificationBuilder();
        if (params.get("brandId") != null) {
            specBuilder.add(carHasBrandId(Integer.parseInt(params.get("brandId"))));
        }
        if (params.get("modelId") != null) {
            specBuilder.add(carHasModelId(Integer.parseInt(params.get("modelId"))));
        }
        if ((params.get("modelYearMin") !=null) && (params.get("modelYearMax") !=null)) {
            specBuilder.add(modelYearBetween(Integer.valueOf(params.get("modelYearMin")),Integer.valueOf(params.get("modelYearMax"))));
        }
        if (params.get("bodyTypeId") != null) {
            specBuilder.add(carHasBodyTypeId(Integer.valueOf(params.get("bodyTypeId"))));
        }
        if (params.get("mileageMax") !=null) {
            specBuilder.add(carHasMileageLessOrEqualThan(Integer.valueOf(params.get("mileageMax"))));
        }
        String engineMinString = params.get("engineMin");
        String engineMaxString = params.get("engineMax");
        if ((engineMinString !=null) || (engineMaxString !=null)) {
            specBuilder.add(engineCapacityBetween(engineMinString != null?Double.valueOf(Double.valueOf(engineMinString)*1000).intValue():null,
                    engineMaxString != null?Double.valueOf(Double.valueOf(engineMaxString)*1000).intValue():null));
        }
        if (params.get("fuelTypeId") !=null){
            specBuilder.add(carHasFuelTypeId(Integer.valueOf(params.get("fuelTypeId"))));
        }
        if (params.get("transmissionTypeId") != null){
            specBuilder.add(carHasTransmissionTypeId(Integer.valueOf(params.get("transmissionTypeId"))));
        }
        if (params.get("wheelDriveId") != null){
            specBuilder.add(carHasWheelDriveId(Integer.valueOf(params.get("wheelDriveId"))));
        }
        return specBuilder.build();
    }

}
