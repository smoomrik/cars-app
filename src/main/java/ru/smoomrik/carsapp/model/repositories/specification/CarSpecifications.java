package ru.smoomrik.carsapp.model.repositories.specification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import ru.smoomrik.carsapp.model.entities.*;

/**
 * Created by smoomrik on 07/10/16.
 */
public class CarSpecifications {
    private static final Logger log = LoggerFactory.getLogger(CarSpecifications.class);

    public static Specification<Car> carHasBrandId(Integer brandId) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(Car_.brand).get(CarBrand_.id), brandId);
    }

    public static Specification<Car> carHasModelId(Integer modelId) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(Car_.model).get(CarModel_.id), modelId);
    }

    public static Specification<Car> modelYearBetween(Integer from, Integer to) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.
                between(root.get(Car_.modelYear), from, to);
    }

    public static Specification<Car> carHasBodyTypeId(Integer bodyTypeId) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(Car_.body).get(CarBody_.id), bodyTypeId);
    }

    public static Specification<Car> carHasMileageLessOrEqualThan(Integer mileageMax) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get(Car_.mileage), mileageMax);
    }

    public static Specification<Car> engineCapacityBetween(Integer from, Integer to) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            if (to == null) {
                return criteriaBuilder.greaterThanOrEqualTo(root.get(Car_.engineCapacity), from);
            } else if (from == null) {
                return criteriaBuilder.lessThanOrEqualTo(root.get(Car_.engineCapacity), to);
            } else return criteriaBuilder.between(root.get(Car_.engineCapacity), from, to);
        };
    }

    public static Specification<Car> carHasFuelTypeId(Integer fuelTypeId) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(Car_.fuel).get(Fuel_.id), fuelTypeId);
    }

    public static Specification<Car> carHasTransmissionTypeId(Integer transmissionId) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(Car_.transmission).get(Transmission_.id), transmissionId);
    }

    public static Specification<Car> carHasWheelDriveId(Integer wheelDriveId) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(Car_.wheelDrive).get(WheelDrive_.id), wheelDriveId);

    }
}
