package ru.smoomrik.carsapp.model.repositories;

import org.springframework.data.repository.Repository;
import ru.smoomrik.carsapp.model.entities.CarBrand;

import java.util.List;
import java.util.Optional;

/**
 * Created by smoomrik on 16/09/16.
 */
public interface CarBrandRepository extends Repository<CarBrand, Long> {
    List<CarBrand> findAll();
    Optional<CarBrand> findById(Long id);

}
