package ru.smoomrik.carsapp.service;

import ru.smoomrik.carsapp.model.entities.Fuel;

import java.util.List;

/**
 * Created by smoomrik on 16/09/16.
 */
public interface FuelService {
    List<Fuel> findAllFuel();
    Fuel findFuelById(Long id);
}
