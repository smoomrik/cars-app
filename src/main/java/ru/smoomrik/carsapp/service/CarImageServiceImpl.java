package ru.smoomrik.carsapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * Created by smoomrik on 31/10/16.
 */
public class CarImageServiceImpl implements CarImageService {
    private static final Logger log = LoggerFactory.getLogger(CarImageServiceImpl.class);
    private final String rootPath;
    private final String separator;

    public CarImageServiceImpl(String rootPath) {
        this.rootPath = rootPath;
        separator = System.getProperty("file.separator");
    }

    @Override
    public byte[] getImageByAdUUIDAndImageUUID(final String adUUID, final String imageUUID) throws FileNotFoundException {
        log.debug("getImageByAdUUIDAndImageUUID(): adUUID = {}, imageUUID = {}", adUUID == null ? null : adUUID, imageUUID == null ? null : imageUUID);
        if (adUUID == null || adUUID.trim().isEmpty() || imageUUID == null || imageUUID.trim().isEmpty())
            throw new IllegalArgumentException();

        final String path = rootPath + separator + adUUID + separator + imageUUID;
        if (Files.notExists(Paths.get(path))) {
            throw new FileNotFoundException("File with uuid " + imageUUID + " not founded at folder " + adUUID);
        } else {
            try {
                return Files.readAllBytes(Paths.get(path));
            } catch (IOException e) {
                log.error("Error was thrown while reading file " + imageUUID + " at directory " + adUUID + ". ", e);
                throw new IllegalStateException(e);
            }
        }
    }

    @Override
    public List<byte[]> getImagesByAdUUID(String adUUID) {
        log.debug("getImagesByAdUUID(): adUUID = {}", adUUID == null ? null : adUUID);
        if (adUUID == null || adUUID.trim().isEmpty()) throw new IllegalArgumentException();
        Path path = Paths.get(rootPath + separator + adUUID);
        if (Files.notExists(path)) {
            log.error("Directory with adUUID = \"{}\" not founded.", adUUID);
            return Collections.emptyList();
        }

        ArrayList<byte[]> result = new ArrayList<>();
        if (path.toFile().isDirectory()) {
            try (Stream<Path> paths = Files.walk(path)) {
                paths.forEach(filePath -> {
                    if (Files.isRegularFile(filePath)) {
                        try {
                            result.add(Files.readAllBytes(filePath));
                        } catch (IOException e) {
                            log.error("Error while reading file " + filePath.toString() + ".", e);
                        }
                    }
                });
            } catch (IOException e) {
                log.error("Error was thrown while retrieve files from directory " + path, e);
                return Collections.emptyList();
            }
        } else throw new IllegalArgumentException(path.toString() + "is not a directory.");

        return result;
    }

    @Override
    public List<String> getImagesUUIDByAdUUID(String adUUID) {
        log.debug("getImagesUUIDByAdUUID(): adUUID = {}", adUUID == null ? null : adUUID);
        if (adUUID == null || adUUID.trim().isEmpty())
            throw new IllegalArgumentException("Param can't be null or empty.");
        Path path = pathBuilder(rootPath, adUUID);
        ArrayList<String> uuids = new ArrayList<>();
        if (Files.isDirectory(path)) {
            try (Stream<Path> paths = Files.walk(path)) {
                paths.forEach(filePath -> {
                    if (Files.isRegularFile(filePath)) {
                        uuids.add(filePath.getFileName().toString());
                    }
                });
                return uuids;
            } catch (IOException e) {
                log.error("Error was thrown while walking path: " + path.toString(), e);
                return uuids;
            }
        } else return uuids;

    }

    @Override
    public Integer getCountImagesByAdUUID(String adUUID) {
        return null;
    }

    @Override
    public String addImage(final String adUUID, final byte[] image) {
        log.debug("addImage(): advert uuid = {}, image size = {}", adUUID, image.length);
        final String adRootPath = rootPath + System.getProperty("file.separator") + adUUID;
        if (Files.notExists(Paths.get(adRootPath))) {
            try {
                Files.createDirectory(Paths.get(adRootPath));
                log.debug("Directory \"{}\" does not exist, and successfully created.", adRootPath);
            } catch (IOException e) {
                log.error("Error was thrown while creating directory:\n\t", e);
            }
        }

        UUID uuidImage = UUID.nameUUIDFromBytes(image);
        final Path imagePath = Paths.get(adRootPath + System.getProperty("file.separator") + uuidImage.toString());
        try {
            Files.write(imagePath, image);
            log.debug("Image successfully uploaded. Absolute path: {}, image size: {}", imagePath.toFile().getAbsolutePath(), imagePath.toFile().length());
        } catch (IOException e) {
            log.error("Error was thrown while writing file:\n\t", e);
            throw new IllegalStateException(e);
        }
        return uuidImage.toString();
    }

    @Override
    public void deleteImage(String adUUID, String imageUUID) throws FileNotFoundException {
        log.debug("deleteImage(): adUUID = {}, imageUUID = {}", adUUID, imageUUID);
        if (Files.exists(pathBuilder(rootPath, adUUID))) {

            Path path = pathBuilder(rootPath, adUUID, imageUUID);
            if (Files.exists(path)) {
                try {
                    Files.delete(path);
                    deleteDirectoryIfEmpty(pathBuilder(rootPath, adUUID));
                } catch (IOException e) {
                    log.error("Error while deleting file: ", e);
                    throw new IllegalStateException(e);
                }
            } else {
                throw new FileNotFoundException("File " + imageUUID + " not founded at directory " + adUUID);
            }
        } else {
            throw new FileNotFoundException("Directory " + adUUID + " not found.");
        }
    }

    String getRootPath() {
        return rootPath;
    }

    String getSeparator() {
        return separator;
    }

    private Path pathBuilder(String... tokens) {
        StringBuilder builder = new StringBuilder();
        for (String token : tokens) {
            builder.append(token).append(separator);
        }
        return Paths.get(builder.toString());
    }

    private void deleteDirectoryIfEmpty(Path path) {
        log.debug("deleteDirectoryIfEmpty(): path = {}", path);
        if (Files.exists(path)) {
            if (Files.isDirectory(path) && (path.toFile() != null) &&(path.toFile().listFiles().length <= 0)) {
                try {
                    Files.delete(path);
                } catch (IOException e) {
                    log.error("Error was thrown while deleting directory " + path);
                    throw new IllegalStateException(e);
                }
            }
        }

    }
}
