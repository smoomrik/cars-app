package ru.smoomrik.carsapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.smoomrik.carsapp.model.entities.Fuel;
import ru.smoomrik.carsapp.model.repositories.FuelRepository;

import java.util.List;

/**
 * Created by smoomrik on 16/09/16.
 */
@Service
public class FuelServiceImpl implements FuelService {
    private  static final Logger log = LoggerFactory.getLogger(FuelServiceImpl.class);
    private final FuelRepository fuelRepository;

    @Override
    public List<Fuel> findAllFuel() {
        return fuelRepository.findAll();
    }

    @Override
    public Fuel findFuelById(Long id) {
        log.debug("findFuelById(): id = {}", id);
        return fuelRepository.findById(id).orElseThrow(()-> new IllegalArgumentException("Could not find Fuel with id = " + id));
    }

    @Autowired
    public FuelServiceImpl(FuelRepository fuelRepository) {
        this.fuelRepository = fuelRepository;
    }
}
