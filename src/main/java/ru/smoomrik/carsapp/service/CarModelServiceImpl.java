package ru.smoomrik.carsapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.smoomrik.carsapp.model.entities.CarModel;
import ru.smoomrik.carsapp.model.repositories.CarModelRepository;

import java.util.List;

/**
 * Created by smoomrik on 16/09/16.
 */
@Service
public class CarModelServiceImpl implements CarModelService {
    private static final Logger log = LoggerFactory.getLogger(CarModelServiceImpl.class);
    private final CarModelRepository carModelRepository;

    @Override
    public List<CarModel> findAllModels() {
        return carModelRepository.findAll();
    }

    @Override
    public List<CarModel> findAllModelsByBrandId(Long brandId) {
        return carModelRepository.findAllCarModelByBrandId(brandId);
    }

    @Override
    public CarModel findModelById(long id) {
        log.debug("findModelById(): id = {}", id);
        return carModelRepository.findById(id).orElseThrow(()-> new IllegalArgumentException("Could not find Car Model with id = " + id ));
    }

    @Autowired
    public CarModelServiceImpl(CarModelRepository carModelRepository) {
        this.carModelRepository = carModelRepository;
    }
}
