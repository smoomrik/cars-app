package ru.smoomrik.carsapp.service;

import ru.smoomrik.carsapp.model.entities.Transmission;

import java.util.List;

/**
 * Created by smoomrik on 16/09/16.
 */
public interface TransmissionService {
    List<Transmission> findAllTransmission();
    Transmission findTransmissionById(Long id);
}
