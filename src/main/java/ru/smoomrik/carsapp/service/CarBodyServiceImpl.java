package ru.smoomrik.carsapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.smoomrik.carsapp.model.entities.CarBody;
import ru.smoomrik.carsapp.model.repositories.CarBodyRepository;

import java.util.List;

/**
 * Created by smoomrik on 16/09/16.
 */
@Service
public class CarBodyServiceImpl implements CarBodyService {

    private final static Logger log = LoggerFactory.getLogger(CarBodyServiceImpl.class);
    private final CarBodyRepository carBodyRepository;
    @Override
    public List<CarBody> findAllCarBody() {
        return carBodyRepository.findAll();
    }

    @Override
    public CarBody findBodyById(long id) {
        log.debug("findBodyById(): id = {}", id);
        return carBodyRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Could not find CarBody with id = " + id));
    }

    @Autowired
    public CarBodyServiceImpl(CarBodyRepository carBodyRepository) {
        this.carBodyRepository = carBodyRepository;
    }
}
