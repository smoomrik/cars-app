package ru.smoomrik.carsapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.smoomrik.carsapp.model.entities.WheelDrive;
import ru.smoomrik.carsapp.model.repositories.WheelDriveRepository;

import java.util.List;

/**
 * Created by smoomrik on 16/09/16.
 */
@Service
public class WheelDriveServiceImpl implements WheelDriveService {
    private static final Logger log = LoggerFactory.getLogger(WheelDriveServiceImpl.class);
    private final WheelDriveRepository wheelDriveRepository;

    @Override
    public List<WheelDrive> findAllWheelDrive() {
        return wheelDriveRepository.findAll();
    }

    @Override
    public WheelDrive findWheelDriveById(Long id) {
        log.debug("findWheelDriveById(): id = {}", id);
        return wheelDriveRepository.findById(id).orElseThrow(()-> new IllegalArgumentException("Could not find WheelDrive with id = " + id));
    }

    @Autowired
    public WheelDriveServiceImpl(WheelDriveRepository wheelDriveRepository) {
        this.wheelDriveRepository = wheelDriveRepository;
    }
}
