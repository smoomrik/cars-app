package ru.smoomrik.carsapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.smoomrik.carsapp.model.entities.Car;
import ru.smoomrik.carsapp.model.repositories.CarRepository;

import java.util.Optional;

/**
 * Created by smoomrik on 16/09/16.
 */
@Service
public class CarServiceImpl implements CarService {
    private static final Logger log = LoggerFactory.getLogger(CarServiceImpl.class);

    private final CarRepository carRepository;
    private final CarBrandService carBrandService;
    private final CarModelService carModelService;
    private final FuelService fuelService;
    private final CarBodyService carBodyService;
    private final TransmissionService transmissionService;
    private final WheelDriveService wheelDriveService;


    @Override
    public Page<Car> getAllCars(Pageable pageable) {
        Page<Car> allCars = carRepository.findAll(pageable);
        return allCars;
    }

    @Override
    public Optional<Car> getCarById(Long id) {
        return carRepository.findCarById(id);
    }

    @Override
    public Car saveCar(Car car){
        if (car == null) throw new IllegalArgumentException("Param can't be null.");
        car.setBrand(carBrandService.findBrandById(car.getBrand().getId()));
        car.setModel(carModelService.findModelById(car.getModel().getId()));
        car.setFuel(fuelService.findFuelById(car.getFuel().getId()));
        car.setBody(carBodyService.findBodyById(car.getBody().getId()));
        car.setTransmission(transmissionService.findTransmissionById(car.getTransmission().getId()));
        car.setWheelDrive(wheelDriveService.findWheelDriveById(car.getWheelDrive().getId()));
        car.setPublishDate(new java.sql.Date(new java.util.Date().getTime()));
        log.debug("Car before save: {}", car);

        return carRepository.save(car);
    }

    @Autowired
    public CarServiceImpl(CarRepository carRepository,
                          CarBrandService carBrandService,
                          CarModelService carModelService,
                          FuelService fuelService,
                          CarBodyService carBodyService,
                          TransmissionService transmissionService,
                          WheelDriveService wheelDriveService
    ) {
        this.carRepository = carRepository;
        this.carBrandService = carBrandService;
        this.carModelService = carModelService;
        this.fuelService = fuelService;
        this.carBodyService = carBodyService;
        this.transmissionService = transmissionService;
        this.wheelDriveService = wheelDriveService;
    }
}
