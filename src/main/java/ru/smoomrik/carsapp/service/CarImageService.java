package ru.smoomrik.carsapp.service;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by smoomrik on 31/10/16.
 */
public interface CarImageService {

    List<byte[]> getImagesByAdUUID(String adUUID);
    List<String> getImagesUUIDByAdUUID(String adUUID);
    byte[] getImageByAdUUIDAndImageUUID(String adUUID, String imageUUID) throws FileNotFoundException;
    Integer getCountImagesByAdUUID(String adUUID);
    String addImage(String adUUID, byte[] image);
    void deleteImage(String adUUID, String imageUUID) throws FileNotFoundException;


}
