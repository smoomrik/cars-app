package ru.smoomrik.carsapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.smoomrik.carsapp.model.entities.Transmission;
import ru.smoomrik.carsapp.model.repositories.TransmissionRepository;

import java.util.List;

/**
 * Created by smoomrik on 16/09/16.
 */
@Service
public class TransmissionServiceImpl implements TransmissionService {
    private static final Logger log = LoggerFactory.getLogger(TransmissionServiceImpl.class);
    private final TransmissionRepository transmissionRepository;

    @Override
    public List<Transmission> findAllTransmission() {
        return transmissionRepository.findAll();
    }

    @Override
    public Transmission findTransmissionById(Long id) {
        log.debug("findTransmissionById(): id = {}", id);
        return transmissionRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Could not find Transmission with id = " + id));
    }

    @Autowired
    public TransmissionServiceImpl(TransmissionRepository transmissionRepository) {
        this.transmissionRepository = transmissionRepository;
    }
}
