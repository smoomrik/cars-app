package ru.smoomrik.carsapp.service;

import ru.smoomrik.carsapp.model.entities.WheelDrive;

import java.util.List;

/**
 * Created by smoomrik on 16/09/16.
 */
public interface WheelDriveService {
    List<WheelDrive> findAllWheelDrive();
    WheelDrive findWheelDriveById(Long id);
}
