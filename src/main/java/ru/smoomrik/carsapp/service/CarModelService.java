package ru.smoomrik.carsapp.service;

import ru.smoomrik.carsapp.model.entities.CarModel;

import java.util.List;

/**
 * Created by smoomrik on 16/09/16.
 */
public interface CarModelService {

    List<CarModel> findAllModels();
    List<CarModel> findAllModelsByBrandId(Long brandId);
    CarModel findModelById(long id);
}
