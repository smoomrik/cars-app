package ru.smoomrik.carsapp.service;

import org.springframework.stereotype.Service;
import ru.smoomrik.carsapp.model.entities.CarBrand;

import java.util.List;

/**
 * Created by smoomrik on 16/09/16.
 */
@Service
public interface CarBrandService {
    List<CarBrand> findAllBrands();
    CarBrand findBrandById(Long id);
}
