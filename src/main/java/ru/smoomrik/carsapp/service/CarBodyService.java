package ru.smoomrik.carsapp.service;

import ru.smoomrik.carsapp.model.entities.CarBody;

import java.util.List;

/**
 * Created by smoomrik on 16/09/16.
 */
public interface CarBodyService {
    List<CarBody> findAllCarBody();
    CarBody findBodyById(long id);
}
