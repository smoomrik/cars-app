package ru.smoomrik.carsapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.smoomrik.carsapp.model.entities.CarBrand;
import ru.smoomrik.carsapp.model.repositories.CarBrandRepository;

import java.util.List;

/**
 * Created by smoomrik on 16/09/16.
 */
@Service
public class CarBrandServiceImpl implements CarBrandService {

    private static final Logger log = LoggerFactory.getLogger(CarBrandServiceImpl.class);

    private final CarBrandRepository carBrandRepository;
    @Override
    public List<CarBrand> findAllBrands() {
        return carBrandRepository.findAll();
    }

    @Override
    public CarBrand findBrandById(Long id) {
        log.debug("FindBrandById(): id = {}", id);
        return carBrandRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Could not find Car Brand with id = " + id));
    }

    @Autowired
    public CarBrandServiceImpl(CarBrandRepository carBrandRepository) {
        this.carBrandRepository = carBrandRepository;
    }


}
