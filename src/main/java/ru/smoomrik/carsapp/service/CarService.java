package ru.smoomrik.carsapp.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.smoomrik.carsapp.model.entities.Car;

import java.util.Optional;

/**
 * Created by smoomrik on 16/09/16.
 */
public interface CarService {
    Page<Car> getAllCars(Pageable pageable);
    Optional<Car> getCarById(Long id);
    Car saveCar(Car car);
}
