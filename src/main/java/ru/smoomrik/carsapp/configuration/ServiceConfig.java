package ru.smoomrik.carsapp.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import ru.smoomrik.carsapp.service.CarImageService;
import ru.smoomrik.carsapp.service.CarImageServiceImpl;

/**
 * Created by smoomrik on 31/10/16.
 */
@Configuration
@ComponentScan("ru.smoomrik.carsapp.service")
@PropertySource(value = "classpath:application.properties")
public class ServiceConfig {
    private static final Logger log = LoggerFactory.getLogger(ServiceConfig.class);

    @Value("${service.carimage.rootpath}") String carImageServiceRootPath;
    @Bean
    public CarImageService carImageService(){
        log.debug("Service config: carImageService with rootPath: {}", carImageServiceRootPath);
        return new CarImageServiceImpl(carImageServiceRootPath);
    }
}
