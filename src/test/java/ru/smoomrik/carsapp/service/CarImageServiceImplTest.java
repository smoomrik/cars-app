package ru.smoomrik.carsapp.service;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.Properties;

import static org.junit.Assert.*;

/**
 * Created by smoomrik on 02/11/16.
 */
public class CarImageServiceImplTest {

    private static CarImageServiceImpl carImageService;

    @Before
    public void setUp() throws Exception {
        Properties properties = new Properties();
        try {
            properties.load(CarImageServiceImplTest.class.getClassLoader().getResourceAsStream("application.properties"));
            carImageService = new CarImageServiceImpl(properties.getProperty("service.carimage.rootpath"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testConfiguration(){
        assertNotNull(carImageService);
        assertEquals(carImageService.getRootPath(),"/storage/cars-app/ad-images");
    }

    @Test(expected = IllegalArgumentException.class)
    public void getImagesByAdUUIDWithNull() throws Exception {
        carImageService.getImagesByAdUUID(null);
    }
    @Test(expected = IllegalArgumentException.class)
    public void getImagesByAdUUIDWithEmptyParam() throws Exception {
        carImageService.getImagesByAdUUID("");
    }
    @Test(expected = IllegalArgumentException.class)
    public void getImagesByAdUUIDWithSpaces() throws Exception {
        carImageService.getImagesByAdUUID("  ");
    }
    @Test
    public void getImagesByAdUUID() throws Exception {
        assertEquals("fsdfsd",carImageService.getImagesByAdUUID(" asd "), Collections.emptyList());
    }
}